package com.kartal.corejdbc;

import com.kartal.corejdbc.models.LineItem;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

public class OrderComponent extends BaseDB {

    public void updateOrderQuantity(int orderNumber, String productCode, int newQuantity) {
        String query = "UPDATE orderDEtails SET quantityOrdered=? WHERE orderNumber=? AND productCode=?";
        try {
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setInt(1, newQuantity);
            preparedStatement.setInt(2, orderNumber);
            preparedStatement.setString(3, productCode);
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
        }
    }

    //JDBC TRANSACTION SAMPLES

    //Transaction Sample Auto Commit
    public int createOrder(int customerNumber, LineItem lineItem) throws Exception {
        try {
            preparedStatement =
                    getConnection().prepareStatement(
                            "INSERT INTO orders " +
                                    "(orderDate, requiredDate, status, customerNumber ) " +
                                    "VALUES (now(),now(),'In Process',?)",

                            preparedStatement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, customerNumber);

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (!resultSet.next())
                return 0;

            int orderNumber = resultSet.getInt(1);

            String sqlStr =
                    "INSERT INTO orderdetails "
                            + "(orderNumber, productCode_x, quantityOrdered, "
                            + "priceEach, orderLineNumber) "
                            + "VALUES (?,?,?,?,?)";

            preparedStatement =
                    getConnection().prepareStatement(sqlStr);

            preparedStatement.setInt(1, orderNumber);
            preparedStatement.setString(2, lineItem.productCode);
            preparedStatement.setInt(3, lineItem.quantityOrdered);
            preparedStatement.setDouble(4, lineItem.priceEach);
            preparedStatement.setDouble(5, 1);

            int count = preparedStatement.executeUpdate();

            if (count == 1) {
                return orderNumber;
            } else {
                return 0;
            }

        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
            return 0;
        }
    }

    //Transaction Sample Manual Commit
    public int createOrderManualCommit(int customerNumber, LineItem lineItem) throws Exception {
        try {
            preparedStatement =
                    getConnection().prepareStatement(
                            "INSERT INTO orders " +
                                    "(orderDate, requiredDate, status, customerNumber ) " +
                                    "VALUES (now(),now(),'In Process',?)",

                            preparedStatement.RETURN_GENERATED_KEYS);

            getConnection().setAutoCommit(false);

            preparedStatement.setInt(1, customerNumber);

            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getGeneratedKeys();

            if (!resultSet.next()) {
                getConnection().rollback();
                return 0;
            }


            int orderNumber = resultSet.getInt(1);

            String sqlStr =
                    "INSERT INTO orderdetails "
                            + "(orderNumber, productCode_x, quantityOrdered, "
                            + "priceEach, orderLineNumber) "
                            + "VALUES (?,?,?,?,?)";

            preparedStatement =
                    getConnection().prepareStatement(sqlStr);

            preparedStatement.setInt(1, orderNumber);
            preparedStatement.setString(2, lineItem.productCode);
            preparedStatement.setInt(3, lineItem.quantityOrdered);
            preparedStatement.setDouble(4, lineItem.priceEach);
            preparedStatement.setDouble(5, 1);

            int count = preparedStatement.executeUpdate();

            if (count == 1) {
                getConnection().commit();
                return orderNumber;
            } else {
                getConnection().rollback();
                return 0;
            }

        } catch (Exception ex) {
            getConnection().rollback();
            throw ex;
        }
    }

    //CachedRowSet Sample
    public CachedRowSet ordersByStatus(String status) throws Exception {

        RowSetFactory rowSetProvider = RowSetProvider.newFactory();
        CachedRowSet rowSet = rowSetProvider.createCachedRowSet();
        rowSet.setUrl("connectionstr");
        rowSet.setString(1, status);
        rowSet.execute();
        return rowSet;
    }

}
