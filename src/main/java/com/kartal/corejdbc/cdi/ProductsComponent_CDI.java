package com.kartal.corejdbc.cdi;

import com.kartal.corejdbc.BaseDB;
import com.kartal.corejdbc.jee.DatasourceProducer;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;

public class ProductsComponent_CDI {

    @Inject
    //@MySqlDataSource
            DataSource dataSource;

    public static void Sample() {
        try {
            ProductsComponent_CDI component = new ProductsComponent_CDI();
            DatasourceProducer producer = new DatasourceProducer();
            component.dataSource = producer.produceDataSource();

            if (component.getProductCount()) {
                System.out.println("Demo : Try to connect with JEE Datasource");
                System.out.println("SUCCESS");
            } else {
                System.out.println("Demo : Try to connect with JEE Datasource");
                System.out.println("FAILED");
            }
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
        }
    }

    public boolean getProductCount() throws Exception {
        Connection connection = BaseDB.getConnection();
        boolean isValid = connection.isValid(2);
        connection.close();
        return isValid;
    }
}
