package com.kartal.corejdbc;

import com.kartal.corejdbc.models.Employee;

import java.sql.*;

public class HrComponent extends BaseDB {

    public int updateOrderQuantity(String managerBeingReplaced, String replacementManager) {
        String query = "UPDATE employees SET reportsTo=? WHERE reportsTo=?";
        try {
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, replacementManager);
            preparedStatement.setString(2, managerBeingReplaced);
            return preparedStatement.executeUpdate();
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
            return 0;
        }
    }

    public int addEmployee(Employee employee) {
        String query = "INSERT INTO employees(lastname,firstName,extension,email,officeCode,jobTitle)" +
                "VALUES(?,?,?,?,?,?)";
        try {
            preparedStatement = getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, employee.lastName);
            preparedStatement.setString(2, employee.firstName);
            preparedStatement.setString(3, employee.extension);
            preparedStatement.setString(4, employee.email);
            preparedStatement.setString(5, employee.officeCode);
            preparedStatement.setString(6, employee.jobTitle);
            preparedStatement.executeUpdate();

            resultSet = preparedStatement.getResultSet();
            int autoGenKey = 0;

            if (resultSet.next())
                autoGenKey = resultSet.getInt(1);

            return autoGenKey;

        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
            return 0;
        }
    }


    public boolean deleteEmployee(String employeeNumber) {
        String query = "DELETE FROM  employees WHERE  employeeNumber=?";
        try {
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, employeeNumber);
            int count = preparedStatement.executeUpdate();
            return count > 0;
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
            return false;
        }
    }

    //Output parameter sample
    public String updateEmail(int employeeNumber, String newEmail) {
        try {
            CallableStatement callableStatement = getConnection().prepareCall("{call updateEmail(?,?)}");
            callableStatement.setInt(1, employeeNumber);
            callableStatement.registerOutParameter(2, Types.NVARCHAR);
            callableStatement.setString(2, newEmail);
            callableStatement.execute();

            return callableStatement.getString(2);
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
            return null;
        }
    }
}
