package com.kartal.corejdbc;

import javax.sql.rowset.CachedRowSet;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Application {

    public static void main(String[] args) {
        // Samplem2c1();
        ProductsComponent pc = new ProductsComponent();
        // pc.printProductList();
        // pc.printProductListWithPreparedStatement(50,100);;

        //new OrderComponent().updateOrderQuantity(10100,"",1);
    }

    static void Samplem2c1() {

        //Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        try {
            ConnectComponent comp = new ConnectComponent();
            if (comp.tryConnection()) {
                System.out.println("Demo m2c1");
                System.out.println("The attempt to Connection was a SUCCESS");
            } else {
                System.out.println("Demo m2c1");
                System.out.println("The attempt to Connection FAILED");
            }
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
        }
    }

    static void CachedRowSet_Sample() throws Exception {
        String status = "In process";
        OrderComponent comp = new OrderComponent();

        try (CachedRowSet rowSet1 = comp.ordersByStatus(status);
             FileOutputStream fout = new FileOutputStream("row_set_serialized.ser");
             ObjectOutputStream oos = new ObjectOutputStream(fout)) {

            oos.writeObject(rowSet1);
            fout.close();
            oos.close();

            try (FileInputStream fin = new FileInputStream("row_set_serialized.ser");
                 ObjectInputStream ois = new ObjectInputStream(fin);
                 CachedRowSet rowSet2 = (CachedRowSet) ois.readObject()) {

                while (rowSet2.next()) {
                    int customerNumber = rowSet2.getInt("customerNumber");
                    int orderNumber = rowSet2.getInt("orderNumber");
                    System.out.println(customerNumber + " " + orderNumber + " " + status);
                }
            }
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
        }
    }
}
