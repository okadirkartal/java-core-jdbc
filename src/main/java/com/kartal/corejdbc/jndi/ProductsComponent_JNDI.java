package com.kartal.corejdbc.jndi;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;

public class ProductsComponent_JNDI {

    public boolean getProductCount() throws Exception {
        InitialContext ctx = new InitialContext();

        DataSource dataSource = (DataSource) ctx.lookup("jdbc/mysql");

        Connection connection = dataSource.getConnection("root", "test");

        boolean isValid = connection.isValid(2);

        connection.close();

        return isValid;
    }
}
