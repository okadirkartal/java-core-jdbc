package com.kartal.corejdbc;

public class ConnectComponent extends BaseDB {
    public boolean tryConnection() {
        try {
            boolean isValid = getConnection().isValid(2);
            getConnection().close();
            return isValid;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
