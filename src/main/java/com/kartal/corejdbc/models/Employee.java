package com.kartal.corejdbc.models;

public class Employee {
    public String lastName;
    public String firstName;
    public String extension;
    public String email;
    public String officeCode;
    public String jobTitle;
}
