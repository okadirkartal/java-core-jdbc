package com.kartal.corejdbc;

import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.CallableStatement;

public class ProductsComponent extends BaseDB {

    public void printProductList() {
        try {
            statement = getConnection().createStatement();
            resultSet = statement.executeQuery("SELECT productName,quantityInStock,buyPrice FROM products");

            while (resultSet.next()) {
                String name = resultSet.getString("productName");
                int quantity = resultSet.getInt("quantityInStock");
                double price = resultSet.getDouble("buyPrice");
                System.out.format("%-45s %5d %10.2f%n", name, quantity, price);
            }

        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);

        } finally {
            try {
                resultSet.close();
            } catch (Exception ex) {
                System.out.println("ResultSet.close(): " + ex.getMessage());
            }
            try {
                statement.close();
            } catch (Exception ex) {
                System.out.println("Statement.close(): " + ex.getMessage());
            }

            try {
                getConnection().close();
            } catch (Exception ex) {
                System.out.println("Connection.close(): " + ex.getMessage());
            }
        }
    }

    public void printProductListWithPreparedStatement(double lowPrice, double highPrice) {
        try {
            preparedStatement = getConnection().prepareStatement("SELECT * FROM products WHERE buyPrice BETWEEN ? AND ?");
            preparedStatement.setDouble(1, lowPrice);
            preparedStatement.setDouble(2, highPrice);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("productName");
                System.out.println(name);
            }
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
        }
    }

    public boolean storeCLOB(String prodLine, InputStreamReader inStream) {
        String sqlString = "UPDATE productLines SET htmlDescription = ? WHERE productLine = ?";
        try {
            preparedStatement = getConnection().prepareStatement(sqlString);
            preparedStatement.setString(2, prodLine);
            preparedStatement.setCharacterStream(1, inStream);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
            return false;
        }
    }

    public void clobSample() {
        try {
            String prodLine = "Planes";
            String fileName = "JanesAllWorldAircraft1913_704482.txt";
            File file = new File(fileName);
            FileReader fileReader = new FileReader(file);

            boolean success = storeCLOB(prodLine, fileReader);
            fileReader.close();

            if (success)
                System.out.print("Success : The text contents of " + fileName + " has been ...");
            else
                System.out.print("Fail : The text contents of " + fileName + " has been ...");
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
        }
    }

    public Reader readCLOB(String prodLine) {
        String sqlString = "SELECT htmlDescription FROM productLines WHERE productLine = ?";
        try {
            preparedStatement = getConnection().prepareStatement(sqlString);
            preparedStatement.setString(1, prodLine);
            resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? resultSet.getCharacterStream(1) :
                    null;
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
            return null;
        }
    }

    //SP Example
    public void listProductsBy(String productLine) throws Exception {
        try {
            CallableStatement callableStatement = getConnection().prepareCall("{call listProductsFor(?)}");

            callableStatement.setString(1, productLine);

            boolean success = callableStatement.execute();

            if (success) {
                resultSet = callableStatement.getResultSet();

                while (resultSet.next()) {
                    System.out.println(resultSet.getString("productName"));
                }
            }
        } catch (Exception ex) {
            util.ExceptionHandler.handleException(ex);
        }
    }
}
