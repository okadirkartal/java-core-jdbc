package com.kartal.corejdbc;

import java.sql.*;

public class BaseDB {

    public static String connectionString = "jdbc:mysql://localhost:3306/classicmodels?user=root&password=test&serverTimezone=UTC";
    private static Connection connection;
    protected Statement statement;

    protected PreparedStatement preparedStatement;

    protected ResultSet resultSet;

    public static Connection getConnection() throws Exception {
        if (connection == null)
            connection = DriverManager.getConnection(connectionString);
        return connection;
    }
}
