package com.kartal.corejdbc.jee;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.sql.DataSource;
import java.sql.Connection;

public class ProductsComponent_JEE implements SessionBean {

    private static final long serialVersionUID = 1L;
    @Resource(name = "jdbc/mySql")
    DataSource dataSource;

    public static void Sample() {
        try {
            ProductsComponent_JEE component = new ProductsComponent_JEE();
            DatasourceProducer producer = new DatasourceProducer();
            component.dataSource = producer.produceDataSource();

            if (component.getProductsCount()) {
                System.out.println("Demo : Try to connect with JEE Datasource");
                System.out.println("SUCCESS");
            } else {
                System.out.println("Demo : Try to connect with JEE Datasource");
                System.out.println("FAILED");
            }
        } catch (Exception exception) {
            util.ExceptionHandler.handleException(exception);
        }
    }

    public boolean getProductsCount() throws Exception {
        Connection connection = dataSource.getConnection();

        boolean isValid = connection.isValid(2);

        connection.close();

        return isValid;
    }

    @Override
    public void setSessionContext(SessionContext sessionContext) throws EJBException {

    }

    @Override
    public void ejbRemove() throws EJBException {

    }

    @Override
    public void ejbActivate() throws EJBException {

    }

    @Override
    public void ejbPassivate() throws EJBException {

    }
}
