package com.kartal.corejdbc.jee;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.enterprise.inject.Produces;
import javax.sql.DataSource;

public class DatasourceProducer {
    public MysqlDataSource mysqlDataSource = null;

    @Produces
    //@MySqlDataSource
    public DataSource produceDataSource() {
        if (mysqlDataSource == null) {
            mysqlDataSource = new MysqlDataSource();
            mysqlDataSource.setPassword("test");
            mysqlDataSource.setUser("root");
            mysqlDataSource.setURL("jdbc:mysql://localhost:3306/classicmodels?serverTimezone=UTC");
        }
        return mysqlDataSource;

    }
}
