package com.kartal.corejdbc;

import java.sql.ResultSet;

public class ProductNavUtility {

    ResultSet resultSet;

    public ProductNavUtility(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public void printForward() throws Exception {
        while (resultSet.next()) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }

    public void printFirst() throws Exception {
        while (resultSet.first()) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }

    public void printLast() throws Exception {
        while (resultSet.last()) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }

    public void printAt(int position) throws Exception {
        if (resultSet.absolute(position)) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }

    public void printRelative(int position) throws Exception {
        if (resultSet.relative(position)) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }

    public void printReverse() throws Exception {
        resultSet.afterLast();
        while (resultSet.previous()) {
            System.out.println(" " + resultSet.getString("productName"));
        }
    }
}
